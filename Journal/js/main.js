﻿var main = {
    init: function () {

        main.elements.logout.on('click', function (e) {

            main.functions.request('/Home/Logout', 'post', {}, function (response) {

                if (response.Data)
                    window.location = '/Welcome';

            });

        });

    },
    elements: {
        logout:$('#logout')
    },
    functions: {
        request: function (url,method,data,cb) {

            $.ajax({
                url: url,
                data: data,
                method: method,
                success: function (response) {

                    if (cb)
                        cb();

                },
                error: function (error) {

                    console.log(error);
                }
            });

        }
    }

}