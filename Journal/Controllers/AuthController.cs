﻿using Journal.DB;
using Journal.Security.Web;
using System;
using System.Linq;
using System.Web.Mvc;
using System.Web.Security;
using static Journal.Models.ViewModel;

namespace Journal.Controllers
{
    [AllowAnonymous]
    public class AuthController : Controller
    {
        // GET: Auth
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(LoginViewModel loginViewModel,string ReturnUrl="")
        {
            if (HttpContext.Request.IsAuthenticated)
            {
                return RedirectToAction("Index", "Diary");
            }

            try
            {
                Owner owner = new Owner();

                using (JournalCs db = new JournalCs())
                {
                    owner = db.Owner.Where(w => w.Email == loginViewModel.Email && w.Password == loginViewModel.Password).FirstOrDefault();
                }

                if (owner != null)
                {
                    AuthenticationHelper.CreateAuthCookie(owner.Id, owner.FirstName, owner.LastName, owner.Email, owner.Password, true);
                    //FormsAuthentication.SetAuthCookie(owner.Email, true);

                    if (ReturnUrl != "")
                    {
                        return Redirect(ReturnUrl);
                    }

                    return RedirectToAction("Index", "Diary");
                }

                TempData["Message"] = "Email or passwor incorrect";

                return RedirectToAction("index");

            }
            catch (Exception)
            {
                return RedirectToAction("index");
            }
        }

        public ActionResult Logout()
        {
            AuthenticationHelper.Logout();

            return RedirectToAction("Index", "Home");
        }
    }
}