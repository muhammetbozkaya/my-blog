﻿using Journal.DB;
using Journal.Helper;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static Journal.Models.ViewModel;

namespace Journal.Controllers
{
    [Authorize]
    public class DiaryController : BaseController
    {
        // GET: Diary
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Add()
        {
            return View(new JournalViewModel());
        }

        public ActionResult All()
        {
            return View();
        }

        public ActionResult GetDate()
        {
            return View();
        }

        [HttpPost]
        public JsonResult InsertSummary(JournalPostModel model)
        {
            if (!ModelState.IsValid)
            {
                return Json(new { Status = false,Message="Please select any" });
            }

            using (JournalCs db = new JournalCs())
            {
                var summary = db.Summary.Add(new Summary
                {
                    PointOfDay = model.DayPoint,
                    PointOfOther = model.OthersPoint,
                    MinePoint = model.OwnPoint,
                    Summary1 = SHA.Encrypt(model.Summary),
                    Title = SHA.Encrypt(model.Title),
                    Emotion = model.EmotionId,
                    EmotionForce = model.EmotionForce,
                    CreatedDate = DateTime.Now,
                });

                try
                {
                    db.SaveChanges();

                }
                catch (Exception ex)
                {

                    throw;
                }


                model.Loses.ForEach(f =>
                {
                    db.DailyActions.Add(new DB.DailyActions
                    {
                        Type = (byte)f.ActionId,
                        Description = SHA.Encrypt(f.Description),
                        Force = f.Force,
                        SummaryId = summary.Id,

                    });
                });

                model.Otherselves.ForEach(f =>
                {
                    db.DailyActions.Add(new DB.DailyActions
                    {
                        Type = (byte)f.ActionId,
                        Description = SHA.Encrypt(f.Description),
                        Force = f.Force,
                        SummaryId = summary.Id,

                    });
                });

                model.Yourself.ForEach(f =>
                {
                    db.DailyActions.Add(new DB.DailyActions
                    {
                        Type = (byte)f.ActionId,
                        Description = SHA.Encrypt(f.Description),
                        Force = f.Force,
                        SummaryId = summary.Id,

                    });
                });

                db.SaveChanges();

            }

            return Json(new { Status = true });
        }

        [HttpPost]
        public JsonResult GetSummary(DateTime date)
        {
            JournalPostModel model = new JournalPostModel();

            using (JournalCs db = new JournalCs())
            {

                Summary summary = db.Summary
                    .Where(w => DbFunctions.TruncateTime(w.CreatedDate.Date) == date.Date)
                    .FirstOrDefault();

                if (summary != null)
                {
                    model.DayPoint = summary.PointOfDay;
                    model.OwnPoint = summary.MinePoint;
                    model.OthersPoint = summary.PointOfOther;
                    model.Summary = summary.Summary1;
                    model.Title = summary.Title;
                    model.Id = summary.Id;

                    summary.DailyActions.ToList().ForEach(f =>
                    {
                        ActionViewModel miniModel = new ActionViewModel();

                        miniModel.ActionId = f.Type;
                        miniModel.Description = f.Description;
                        miniModel.Force = f.Force;

                        model.Actions.Add(miniModel);
                    });
                }

            }

            return Json(new { data = model });
        }
    }
}