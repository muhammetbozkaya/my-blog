﻿using Journal.DB;
using Journal.Helper;
using System;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using static Journal.Models.ViewModel;

namespace Journal.Controllers
{
    [AllowAnonymous]
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            return View();
        }
    }
}