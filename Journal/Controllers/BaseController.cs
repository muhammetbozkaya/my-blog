﻿using Journal.DB;
using System.Linq;
using System.Web.Mvc;

namespace Journal.Controllers
{
    public class BaseController : Controller
    {
        public Owner CurrentUser { get; set; }

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (filterContext.HttpContext.Request.IsAuthenticated)
            {
                string currentUserEmail = filterContext.HttpContext.User.Identity.Name;

                using (JournalCs db = new JournalCs())
                {
                    CurrentUser = db.Owner.Where(w => w.Email == currentUserEmail).FirstOrDefault();
                }
            }
            else
            {
                filterContext.HttpContext.Response.Redirect("/Auth/");
            }
            

            base.OnActionExecuting(filterContext);
        }
    }
}