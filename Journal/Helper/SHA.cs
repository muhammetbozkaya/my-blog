﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace Journal.Helper
{
    public class SHA
    {
        public static string Key { get; set; } = "@??z???????V???xr?b????*?w?";

        //private static AesManaged aes { get; set; } = new AesManaged();

        //public static string Encrypt(string data)
        //{
        //    byte[] encrypted;
        //    ICryptoTransform encrypt = aes.CreateEncryptor(Key, aes.IV);

        //    using (MemoryStream ms = new MemoryStream())
        //    {
        //        using (CryptoStream cs = new CryptoStream(ms, encrypt, CryptoStreamMode.Write))
        //        {
        //            byte[] utfData = Encoding.Unicode.GetBytes(data);

        //            cs.Write(utfData, 0, utfData.Length);
        //            cs.Close();
        //        }

        //        encrypted = ms.ToArray();
        //    }

        //    return System.Text.Encoding.ASCII.GetString(encrypted);
        //}

        //public static string Decrypt(string cipherText)
        //{
        //    string plaintext = null;

        //    byte[] cipherData = Convert.FromBase64String(cipherText);

        //    // Create a decryptor    
        //    ICryptoTransform decryptor = aes.CreateDecryptor(Key, aes.IV);
        //    // Create the streams used for decryption.    
        //    using (MemoryStream ms = new MemoryStream(cipherData))
        //    {
        //        // Create crypto stream    
        //        using (CryptoStream cs = new CryptoStream(ms, decryptor, CryptoStreamMode.Write))
        //        {

        //            cs.Write(cipherData, 0, cipherText.Length);

        //            cs.Close();
        //        }

        //        plaintext = Encoding.Unicode.GetString(ms.ToArray());
        //    }

        //    return plaintext;
        //}

        //public static byte[] GenerateKey()
        //{
        //    aes.GenerateKey();

        //    return aes.Key;
        //}

        public static string Encrypt(string plainText)
        {
            if (plainText == null)
            {
                return null;
            }
            
            // Get the bytes of the string
            var bytesToBeEncrypted = Encoding.UTF8.GetBytes(plainText);
            var passwordBytes = Encoding.UTF8.GetBytes(Key);

            // Hash the password with SHA256
            passwordBytes = SHA256.Create().ComputeHash(passwordBytes);

            var bytesEncrypted = Encrypt(bytesToBeEncrypted, passwordBytes);

            return Convert.ToBase64String(bytesEncrypted);
        }

        /// <summary>
        /// Decrypt a string.
        /// </summary>
        /// <param name="encryptedText">String to be decrypted</param>
        /// <param name="password">Password used during encryption</param>
        /// <exception cref="FormatException"></exception>
        public static string Decrypt(string encryptedText)
        {
            if (encryptedText == null)
            {
                return null;
            }
            

            // Get the bytes of the string
            var bytesToBeDecrypted = Convert.FromBase64String(encryptedText);
            var passwordBytes = Encoding.UTF8.GetBytes(Key);

            passwordBytes = SHA256.Create().ComputeHash(passwordBytes);

            var bytesDecrypted = Decrypt(bytesToBeDecrypted, passwordBytes);

            return Encoding.UTF8.GetString(bytesDecrypted);
        }

        private static byte[] Encrypt(byte[] bytesToBeEncrypted, byte[] passwordBytes)
        {
            byte[] encryptedBytes = null;

            // Set your salt here, change it to meet your flavor:
            // The salt bytes must be at least 8 bytes.
            var saltBytes = new byte[] { 1, 2, 3, 4, 5, 6, 7, 8 };

            using (MemoryStream ms = new MemoryStream())
            {
                using (RijndaelManaged AES = new RijndaelManaged())
                {
                    var key = new Rfc2898DeriveBytes(passwordBytes, saltBytes, 1000);

                    AES.KeySize = 256;
                    AES.BlockSize = 128;
                    AES.Key = key.GetBytes(AES.KeySize / 8);
                    AES.IV = key.GetBytes(AES.BlockSize / 8);

                    AES.Mode = CipherMode.CBC;

                    using (var cs = new CryptoStream(ms, AES.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(bytesToBeEncrypted, 0, bytesToBeEncrypted.Length);
                        cs.Close();
                    }

                    encryptedBytes = ms.ToArray();
                }
            }

            return encryptedBytes;
        }

        private static byte[] Decrypt(byte[] bytesToBeDecrypted, byte[] passwordBytes)
        {
            byte[] decryptedBytes = null;

            // Set your salt here, change it to meet your flavor:
            // The salt bytes must be at least 8 bytes.
            var saltBytes = new byte[] { 1, 2, 3, 4, 5, 6, 7, 8 };

            using (MemoryStream ms = new MemoryStream())
            {
                using (RijndaelManaged AES = new RijndaelManaged())
                {
                    var key = new Rfc2898DeriveBytes(passwordBytes, saltBytes, 1000);

                    AES.KeySize = 256;
                    AES.BlockSize = 128;
                    AES.Key = key.GetBytes(AES.KeySize / 8);
                    AES.IV = key.GetBytes(AES.BlockSize / 8);
                    AES.Mode = CipherMode.CBC;

                    using (var cs = new CryptoStream(ms, AES.CreateDecryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(bytesToBeDecrypted, 0, bytesToBeDecrypted.Length);
                        cs.Close();
                    }

                    decryptedBytes = ms.ToArray();
                }
            }

            return decryptedBytes;
        }


    }
}


