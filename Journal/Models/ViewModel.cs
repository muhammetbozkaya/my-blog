﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Journal.Models
{
    public class ViewModel
    {
        public class JournalPostModel
        {
            [Required(ErrorMessage = "Please enter summary")]
            public string Summary { get; set; }
            [Required(ErrorMessage = "Please enter summary")]
            public string Title { get; set; }
            [Required(ErrorMessage = "Please enter day point")]
            public int DayPoint { get; set; }
            [Required(ErrorMessage = "Please enter own point")]
            public int OwnPoint { get; set; }
            [Required(ErrorMessage = "Please enter other point")]
            public int OthersPoint { get; set; }
            public int Id { get; set; }
            [Required(ErrorMessage = "Please enter emotion")]
            public int EmotionId { get; set; }
            [Required(ErrorMessage = "Please enter emotion force")]
            public int EmotionForce { get; set; }
            [Required(ErrorMessage = "Please enter yourself")]
            public List<ActionViewModel> Yourself { get; set; }
            [Required(ErrorMessage = "Please enter otherself")]
            public List<ActionViewModel> Otherselves { get; set; }
            [Required(ErrorMessage = "Please enter loses")]
            public List<ActionViewModel> Loses { get; set; }
            [Required(ErrorMessage = "Please enter actions")]
            public List<ActionViewModel> Actions { get; set; }

        }

        public class ActionViewModel
        {
            public int ActionId { get; set; }
            public int Force { get; set; }
            public string Description { get; set; }
        }

        public class LoginViewModel
        {
            public string Email { get; set; }
            public string Password { get; set; }
        }

        public class JournalViewModel
        {
            private List<SelectListItem> ForceList { get; set; } = new List<SelectListItem>() {
                new SelectListItem { Text = "1", Value = "1" },
                new SelectListItem { Text = "2", Value = "2" },
                new SelectListItem { Text = "3", Value = "3" },
                new SelectListItem { Text = "4", Value = "4" },
                new SelectListItem { Text = "5", Value = "5" }
            };

            public List<SelectListItem> DayPoints { get { return ForceList; } set { OwnPoints = value; } }
            public List<SelectListItem> OwnPoints { get { return ForceList; } set { OwnPoints = value; } }
            public List<SelectListItem> OthersPoint { get { return ForceList; } set { OthersPoint = value; } }
            [Description("Title Of Day")]
            [DisplayName("Title Of Day")]
            public string Title { get; set; }
            public string Summary { get; set; }
            [DisplayName("What made for yourself are you?")]
            public string ForYourself { get; set; }
            [DisplayName("What made for otherselves are you?")]
            public string ForOtherself { get; set; }
            [DisplayName("Loses")]
            public string Lose { get; set; }
            public List<SelectListItem> YourselfForce { get { return ForceList; } set { YourselfForce = value; } }
            public List<SelectListItem> OtherselfForce { get { return ForceList; } set { OtherselfForce = value; } }
            public List<SelectListItem> LoseForce { get { return ForceList; } set { LoseForce = value; } }
            [DisplayName("Emotion")]        
            public List<SelectListItem> Emotion { get { return ForceList; } set { Emotion = value; } }
            public List<SelectListItem> EmotionForce { get { return ForceList; } set { EmotionForce = value; } }




        }
    }
}