﻿using System.Web;
using System.Web.Mvc;

namespace Journal.Filter
{
    public class ActionAttribute : AuthorizeAttribute
    {
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            if (!httpContext.Request.IsAuthenticated) { return false; }

            return true;
        }

    }
}